<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Pogoda</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
      <script>
          $( document ).ready(function() {
              $(".check").click(function(){
                  var city = $('#city').find(":selected").text();
                  $.ajax({
                      url: "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=93173da01acd865f3e4fcada3d62db81&units=metric",
                      success: function(result){
                          $("#icon").attr('src', "http://openweathermap.org/img/w/" + result.weather[0].icon +".png");
                          $("#zachmurzenie").text(result.clouds.all);
                          $("#temperatura").text((result.main.temp));
                          $("#cisnienie").text(result.main.pressure);
                          $("#predkosc_wiatru").text(result.wind.speed);
                      }
                  });
              });
          });
      </script>
  </head>
  <body>
  <h2>Sprawdź pogodę w swoim mieście</h2>
  <select id="city">
    <option value="warszawa">Warszawa</option>
    <option value="gdańsk">Gdańsk</option>
    <option value="kraków">Kraków</option>
    <option value="wrocław">Wrocław</option>
    <option value="poznań">Poznań</option>
    <option value="łódź">Łódź</option>
    <option value="katowice">Katowice</option>
  </select>
  <button class="check">Sprawdź</button><br>
  <img id="icon">
<div id ="pogoda">

  Poziom zachmurzenia: <label id="zachmurzenie"></label><br>
Temperatura <label id="temperatura"></label><br>
Ciśnienie <label id="cisnienie"></label><br>
Predkosc wiatru <label id="predkosc_wiatru"></label><br>

</div>
  </body>
</html>




